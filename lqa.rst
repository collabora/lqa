===
lqa
===

--------------------------------------------------
command line tool for LAVA quality assurance tasks
--------------------------------------------------

:Author: Luis Araujo <luis.araujo@collabora.co.uk>, Andrew Shadura <andrew.shadura@collabora.co.uk>
:Date:   2017-01-05
:Version: 20161215.0
:Manual section: 1
:Manual group: LAVA

SYNOPSIS
========

**lqa** [-h] [-c `CONFIG.yaml`] [--log-file=\ `LOG_FILE`] `COMMAND` …

DESCRIPTION
===========

**lqa**, LAVA QA Tool, is a command line tool for LAVA quality assurance tasks.

*TBE*

OPTIONS
=======

The following options are common for all commands:

--log-file LOG_FILE         Write logs to `LOG_FILE`.
-c <CONFIG.yaml>, --config=<CONFIG.yaml>
                            Use configuration from `CONFIG.yaml`.
-h, --help                  Show a short help message and exit.


COMMANDS
========

submit
    Submit job files

    **lqa submit** [-h] [-g `PROFILE.yaml`] [-n] [-p `PROFILE`] [--all-profiles] [-t `TEMPLATE_VARS`] [-v] [--wait [`TIMEOUT`]] [--debug-vars] [--priority {high,medium,low}] [--check-image-url] [`JOB_FILE.{yaml,json}` [`JOB_FILE.{yaml,json}` ...]]

	-h, --help            show this help message and exit
	-g <PROFILE.yaml>, --profile-file <PROFILE.yaml>
						  set profile file
	-n, --dry-run         dry-run, do everything apart from submitting
	-p PROFILE, --profile PROFILE
						  specify the profiles to use (can be given multiple
						  times)
	--all-profiles        process all the available profiles
	-t TEMPLATE_VARS, --template-vars TEMPLATE_VARS
						  set 'field:value' template variables/values (can be
						  given multiple times)
	-v, --verbose         verbose mode (e.g. print the resulting json)
	--wait TIMEOUT        wait for submitted jobs to complete using a TIMEOUT
						  value (Default timeout of 3 hours)
	--debug-vars          debug undefined template variables
	--priority <high,medium,low>
						  set the job priority
	--check-image-url     check that the image url exists before submitting job

cancel
    Cancel job id
resubmit
    Resubmit job id
mkstream
    Create bundle stream
wait
    Wait for job id
status
    Show job id status
output
    Fetch job id output log
job
    Get information for job id
test
    Show test information
diffresults
    Show test results differences
report
    Generate report for job id's
results
    Get (raw) results
jobdef
    Show job definition file
queue
    Show the current queue of running and submitted jobs
cleanqueue
    Clean jobs queue
maint
    Put the given device in maintenance mode
online
    Put the given device into online mode
devices
    Show status of all available devices
liststreams
    Show streams the user has access to
whoami
    Show authenticated user name
sversion
    Show LAVA server version


SEE ALSO
========

**...**\(1)
