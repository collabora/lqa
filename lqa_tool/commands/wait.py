###################################################################################
# LAVA QA tool
# Copyright (C) 2015, 2016 Collabora Ltd.

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from lqa_api.job import Job
from lqa_api.waitqueue import WaitQueue
from lqa_api.exit_codes import APPLICATION_ERROR
from lqa_tool.commands import Command
from lqa_tool.settings import lqa_logger
from lqa_tool.utils import print_add_msg, print_wait_msg, \
    print_remove_msg, print_timeout_msg

class WaitCmd(Command):

    def __init__(self, args):
        Command.__init__(self, args)
        self.waitq = WaitQueue()

    def run(self):
        for job_id in self.job_ids:
            self.waitq.addjob(Job(job_id, self.server), print_add_msg)
        try:
            self.waitq.wait(self.args.timeout, print_wait_msg,
                            print_remove_msg, print_timeout_msg)
            exit(self.waitq.exit_code)
        except ValueError as e:
            lqa_logger.error("wait timeout: {}".format(e))
            exit(APPLICATION_ERROR)
