###################################################################################
# LAVA QA tool
# Copyright (C) 2015  Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from __future__ import print_function
from lqa_tool.commands import Command

class ListStreamsCmd(Command):

    def __init__(self, args):
        Command.__init__(self, args)

    def run(self):
        for stream in self.server.streams():
            print("{}{}{}{}{}" \
                .format(stream['pathname'],
                        stream['name'] and ' | name: ' + stream['name'],
                        stream['user'] and ' | user: ' + stream['user'],
                        stream['group'] and ' | group: ' + stream['group'],
                        str(stream['bundle_count']) and 
                        ' | bundles: ' + str(stream['bundle_count'])))
